let coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Molestias voluptatem nihil at reiciendis eos dicta reprehenderit repellendus rerum exercitationem? Repudiandae deserunt culpa voluptates et animi sit sapiente, adipisci vitae libero.",
		price: 25000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Molestias voluptatem nihil at reiciendis eos dicta reprehenderit repellendus rerum exercitationem? Repudiandae deserunt culpa voluptates et animi sit sapiente, adipisci vitae libero.",
		price: 35000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Molestias voluptatem nihil at reiciendis eos dicta reprehenderit repellendus rerum exercitationem? Repudiandae deserunt culpa voluptates et animi sit sapiente, adipisci vitae libero.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "NodeJS-ExpressJS",
		description: "Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Molestias voluptatem nihil at reiciendis eos dicta reprehenderit repellendus rerum exercitationem? Repudiandae deserunt culpa voluptates et animi sit sapiente, adipisci vitae libero.",
		price: 55000,
		onOffer: false
	}
]

export default coursesData;
