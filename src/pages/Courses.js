
import coursesData from './../mockData/courses';
import Course from './../components/Course';

export default function Course1(){


	const courses = coursesData.map(course => {

		return <Course key={course.id} courseProp={course}/>
	})
	
	return(
		courses
	)
}
